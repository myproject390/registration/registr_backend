package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/registration/registr_backend/api"
	"gitlab.com/registration/registr_backend/pkg/connections"
	"gitlab.com/registration/registr_backend/pkg/middleware"
	"gitlab.com/registration/registr_backend/pkg/utils"
	"go.uber.org/zap"

	"github.com/blendle/zapdriver"
	"gitlab.com/registration/registr_backend/service"
	rds "gitlab.com/registration/registr_backend/storage/redis"

	"gitlab.com/registration/registr_backend/configs"
	// devicerepo "gitlab.com/golang-team-template/monolith/storage/device"
	userrepo "gitlab.com/registration/registr_backend/storage/users"

	_ "gitlab.com/registration/registr_backend/api/docs" //init swagger docs

	"github.com/gorilla/mux"

	_ "github.com/lib/pq"
)

// @title Monolith Sample API
// @version 1.0
// @description This is a sample MONOLITHAPP server.
// @termsOfService

// @contact.name API Support
// @contact.url https:/najotalim.uz/
// @contact.email farruxismoilov33@gmail.com

// @license.name Apache Licence
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @BasePath /api
func main() {
	// =========================================================================
	// Config
	conf := configs.Load()
	if err := conf.Validate(); err != nil {
		panic(err)
	}

	// Logger
	logger, err := zapdriver.NewDevelopment() // with `development` set to `true`
	if err != nil {
		panic(err)
	}
	// =========================================================================

	// Postgres
	pgPool := connections.Postgres()
	defer pgPool.Close()
	// =========================================================================

	// Redis
	rdsPool := connections.Redis()
	defer rdsPool.Close()
	// =========================================================================

	// Migrations
	utils.MigrateUp()
	// =========================================================================

	// Storage
	userRepo := userrepo.NewRepository(pgPool)
	// deviceRepo := devicerepo.NewRepository(pgPool)
	redisRepo := rds.NewRedisRepo(rdsPool)
	// =========================================================================

	// Sevice
	userService := service.NewUserService(userRepo)
	// deviceService := service.NewDeviceService(deviceRepo)
	// =========================================================================

	// HTTP
	root := mux.NewRouter()
	// =========================================================================

	//Middleware
	root.Use(middleware.PanicRecovery)
	root.Use(middleware.Logging)
	casbinJWTRoleAuthorizer, err := middleware.NewCasbinJWTRoleAuthorizer(&conf)
	if err != nil {
		logger.Fatal("Could not initialize Cabin JWT Role Authorizer", zap.Error(err))
	}
	root.Use(casbinJWTRoleAuthorizer.Middleware)

	// API
	api.Init(root, userService, redisRepo, logger)

	errChan := make(chan error, 1)
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)

	httpServer := http.Server{
		Addr:    conf.HTTPPort,
		Handler: root,
	}

	// http server
	go func() {
		errChan <- httpServer.ListenAndServe()
	}()

	// Blocking main and waiting for shutdown.
	select {
	case err := <-errChan:
		logger.Fatal("error: ", zap.Error(err))

	case <-osSignals:
		logger.Info("main : recieved os signal, shutting down")
		_ = httpServer.Shutdown(context.Background())
		return
	}
	fmt.Println("server run: ", conf.HTTPPort)
}
