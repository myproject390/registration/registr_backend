CREATE TABLE users(
    id UUID NOT NULL PRIMARY KEY,
    first_name VARCHAR(32),
    last_name VARCHAR(32),
    email VARCHAR(256) NOT NULL,
    password VARCHAR(256) NOT NULL
);