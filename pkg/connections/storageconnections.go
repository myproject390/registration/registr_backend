package connections

import (
	"context"
	"fmt"
	"log"
	"sync"

	"github.com/gomodule/redigo/redis"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/registration/registr_backend/configs"
	"gitlab.com/registration/registr_backend/pkg/utils"
)

// // Container ...
// type Container struct {
// 	Redis repo.InMemoryStorage
// 	// S3    *s3.S3
// }

var (
	config           = configs.Load()
	instanceRedis    *redis.Pool
	onceRedis        sync.Once
	instancePostgres *pgxpool.Pool
	oncePostgres     sync.Once
)

// Redis - get Redis instance
func Redis() *redis.Pool {
	onceRedis.Do(func() {
		instanceRedis = redisPool()
	})

	return instanceRedis
}

func redisPool() *redis.Pool {

	pool := redis.Pool{
		// Maximum number of idle connections in the pool.
		MaxIdle: 80,
		// max number of connections
		MaxActive: 12000,
		// Dial is an application supplied function for creating and
		// configuring a connection.
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", fmt.Sprintf("%s:%d", config.RedisHost, config.RedisPort))
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}

	return &pool
}

// Postgres ...
func Postgres() *pgxpool.Pool {

	oncePostgres.Do(func() {
		instancePostgres = pgxPool()
	})

	return instancePostgres
}

func pgxPool() *pgxpool.Pool {

	url, err := utils.ConnectionURLBuilder("postgres")
	if err != nil {
		log.Println("Error generating postgres connection url: ", err.Error())
	}

	pgPool, err := pgxpool.Connect(context.Background(), url)
	if err != nil {
		log.Fatal("error in connection to postgres: ", err)
	}

	return pgPool
}

// // Get - get Container instance
// func Get() *Container {
// 	once.Do(func() {
// 		awsConfig := &aws.Config{
// 			Region:           aws.String("ap-northeast-2"),
// 			Credentials:      credentials.NewStaticCredentials(config.AwsS3Id, config.AwsS3Secret, ""),
// 			DisableSSL:       aws.Bool(false),
// 			S3ForcePathStyle: aws.Bool(true),
// 		}
// 		s3Session := s3.New(session.Must(session.NewSession()), awsConfig)
// 		instance = &Container{
// 			Redis: redisPool(),
// 			S3:    s3Session,
// 		}
// 	})

// 	return instance
// }
